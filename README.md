# springboot-data-jpa-demo

#### 介绍
spring-boot-data-jpa-demo


#### 安装教程

1.  jdk 1.8+
2.  springboot 2.x
3.  maven 3+

#### 使用说明

1.  mvn install -DskipTests
2.  mvn spring-boot:run

