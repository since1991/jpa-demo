package com.jade.demo.product;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Jade
 */
@Entity
@Data
@Table(name = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 901353710172209585L;

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "product_id", nullable = false)
    private Long productId;

    /**
     * 名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 价格，单位：分
     */
    @Column(name = "price")
    private Long price;

    /**
     * 分类
     */
    @Column(name = "type")
    private Integer type;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @LastModifiedDate
    @Column(name = "update_time")
    private Date updateTime;
}