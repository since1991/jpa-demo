package com.jade.demo.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Jade
 */
public interface UserInfoRepository extends
        JpaRepository<UserInfo, Long>,
        JpaSpecificationExecutor<UserInfo>{

    /**
     * 根据手机号查询用户
     * @param phone 手机号
     * @return {@link UserInfo}
     */
    UserInfo findUserInfoByPhone(String phone);
}