package com.jade.demo.user;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Jade
 */
@Entity
@Data
@Table(name = "user_info")
@EntityListeners(AuditingEntityListener.class)
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 6341592803427926740L;

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_info_id", insertable = false, nullable = false)
    private Long userInfoId;

    /**
     * 姓名
     */
    @Column(name = "name")
    private String name;

    /**
     * 电话
     */
    @Column(name = "phone")
    private String phone;

    /**
     * 性别
     */
    @Column(name = "sex")
    private Integer sex;

    /**
     * 创建时间
     */
    @CreatedDate
    @Column(name = "create_time")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @LastModifiedDate
    @Column(name = "update_time")
    private LocalDateTime updateTime;


}