package com.jade.demo.user;

import com.jade.demo.JpaDemoApplicationTests;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Jade
 * @since 2019/12/5
 */
public class UserInfoTests extends JpaDemoApplicationTests {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Test
    public void add() {
        List<UserInfo> users = new ArrayList<>((int)(10 / 0.75));
        for (int i = 0; i < 10; i++) {
            UserInfo userInfo = new UserInfo();
            userInfo.setName("Jade" + i);
            userInfo.setPhone("1843219641" + i);
            userInfo.setSex(1);
            users.add(userInfo);
        }
        userInfoRepository.saveAll(users);
    }

    @Test
    public void delete() {
        Optional<UserInfo> byId = userInfoRepository.findById(1L);
        byId.ifPresent(userInfo -> userInfoRepository.delete(userInfo));
    }

    @Test
    public void update() {
        UserInfo userInfo = userInfoRepository.findUserInfoByPhone("18732196415");
        if (Objects.nonNull(userInfo)) {
            userInfo.setName("Jay1");
            userInfoRepository.save(userInfo);
        }
        System.out.println(userInfo.toString());
    }

    @Test
    public void find() {
        UserInfo userInfo = new UserInfo();
        userInfo.setName("jade");
        userInfo.setSex(1);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.startsWith());
        userInfoRepository.findAll(Example.of(userInfo, matcher),
                PageRequest.of(0, 3, Sort.by(Sort.Order.desc("updateTime"))))
                .forEach(System.out::println);
    }

    @Test
    public void find2() {
        userInfoRepository.findAll((Specification<UserInfo>) (root, query, criteriaBuilder) ->
                criteriaBuilder.and(criteriaBuilder.like(root.get("name"), "jade%"),
                        criteriaBuilder.equal(root.get("sex"), 1)),
                PageRequest.of(0, 3, Sort.by(Sort.Order.desc("updateTime"))))
                .forEach(System.out::println);
    }
}
